import './index.css'
const FormSummary = ({ formData }) => {
  const filteredData = Object.fromEntries(
    Object.entries(formData).filter(([key, value]) => {
      if (Array.isArray(value)) {
        return value.length > 0;
      } else {
        return value !== "";
      }
    })
  );
    return (
      <div className="form-summary">
        <pre>{JSON.stringify(filteredData, null, 2)}</pre>
      </div>
    );
  };
export default FormSummary