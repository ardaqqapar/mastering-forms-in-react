import React, { useState, useEffect } from "react";
import FormSummary from "./FormSummary";
import './index.css';

const UserForm = () => {

  const initialFormData = {
    firstName: "",
    lastName: "",
    age: "",
    employed: false,
    favoriteColor: "",
    sauces: [],
    bestStooge: "Larry",
    notes: ""
  }
  const [formData, setFormData] = useState(initialFormData);
  const [formChanged, setFormChanged] = useState(false);

  useEffect(() => {
    // Compare initial form data with current form data
    const formChanged =
      JSON.stringify(formData) !== JSON.stringify(initialFormData);
    setFormChanged(formChanged);
  }, [formData, initialFormData]);

  const [formErrors, setFormErrors] = useState({
    firstName: false,
    lastName: false,
    age: false,
    notes: false
  });

  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [isFormDirty, setIsFormDirty] = useState(false);

  const handleInputChange = (event) => {
    const { name, value, type, checked } = event.target;
  
    if (type === "checkbox") {
      if (name === "sauces") {
        let updatedSauces = [...formData.sauces];
  
        if (checked) {
          updatedSauces.push(value);
        } else {
          updatedSauces = updatedSauces.filter((sauce) => sauce !== value);
        }
  
        setFormData((prevFormData) => ({
          ...prevFormData,
          sauces: updatedSauces,
        }));
      } else {
        setFormData((prevFormData) => ({
          ...prevFormData,
          [name]: checked,
        }));
      }
    } else {
      setFormData((prevFormData) => ({
        ...prevFormData,
        [name]: value,
      }));
    }
  
    if (isFormSubmitted) {
      setIsFormDirty(true);
    }
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    setIsFormSubmitted(true);
    setIsFormDirty(false);

    const formIsValid = validateForm();

    if (formIsValid) {
      // Display form values using alert()
      alert(JSON.stringify(formData));
    }
  };

  const validateForm = () => {
    const { firstName, lastName, age, notes } = formData;
    const errors = {
      firstName: false,
      lastName: false,
      age: false,
      notes: false
    };

    // Validation for first name
    if (!/^[a-zA-Z\s]+$/.test(firstName)) {
      errors.firstName = true;
    }

    // Validation for last name
    if (!/^[a-zA-Z\s]+$/.test(lastName)) {
      errors.lastName = true;
    }

    // Validation for age
    if (!/^\d+$/.test(age)) {
      errors.age = true;
    }

    // Validation for notes
    if (notes.length > 100) {
      errors.notes = true;
    }

    setFormErrors(errors);

    return !errors.firstName && !errors.lastName && !errors.age && !errors.notes;
  };

  const handleFormReset = () => {
    setFormData({
      firstName: "",
      lastName: "",
      age: "",
      employed: false,
      favoriteColor: "",
      sauces: [],
      bestStooge: "Larry",
      notes: ""
    });

    setFormErrors({
      firstName: false,
      lastName: false,
      age: false,
      notes: false
    });

    setIsFormSubmitted(false);
    setIsFormDirty(false);
  };
  return(
    <div className="userForm">
      <form onSubmit={handleFormSubmit} onReset={handleFormReset}>
        <div className="dataRow">
          <div className="labelName">
            <label htmlFor="firstName">First Name:</label>
          </div>
          <div className="inputData">
            <input
              placeholder="First Name"
              type="text"
              id="firstName"
              name="firstName"
              value={formData.firstName}
              onChange={handleInputChange}
              className={formErrors.firstName ? "error" : ""}
            />
            {formErrors.firstName && <span className="error-text">Please enter a valid first name</span>}
          </div>
        </div>
        <div className="dataRow">
          <div className="labelName">
            <label htmlFor="lastName">Last Name:</label>
          </div>
          <div className="inputData">
            <input
              placeholder="Last Name"
              type="text"
              id="lastName"
              name="lastName"
              value={formData.lastName}
              onChange={handleInputChange}
              className={formErrors.lastName ? "error" : ""}
            />
            {formErrors.lastName && <span className="error-text">Please enter a valid last name</span>}
          </div>
        </div>
        <div className="dataRow">
          <div className="labelName">
            <label htmlFor="age">Age:</label>
          </div>
          <div className="inputData">
            <input
              placeholder="Age"
              type="text"
              id="age"
              name="age"
              value={formData.age}
              onChange={handleInputChange}
              className={formErrors.age ? "error" : ""}
            />
            {formErrors.age && <span className="error-text">Please enter a valid age</span>}
          </div>
        </div>
        <div className="dataRow">
          <div className="labelName">
            <label htmlFor="employed">Employed:</label>
          </div>
          <div className="inputData">
            <input
              type="checkbox"
              id="employed"
              name="employed"
              checked={formData.employed}
              onChange={handleInputChange}
            />
          </div>
        </div>
        <div className="dataRow">
          <div className="labelName">
            <label htmlFor="favoriteColor">Favorite Color:</label>
          </div>
          <div className="inputData">
            <select
              id="favoriteColor"
              name="favoriteColor"
              value={formData.favoriteColor}
              onChange={handleInputChange}
            >
              <option value="">--Please choose an option--</option>
              <option value="red">Red</option>
              <option value="green">Green</option>
              <option value="blue">Blue</option>
            </select>
          </div>
        </div>
        <div className="dataRow">
          <div className="labelName">
            <label>Sauces:</label>
          </div>
          <div className="inputData">
            <div>
              <input
                type="checkbox"
                id="mustard"
                name="sauces"
                value="mustard"
                checked={formData.sauces.includes("mustard")}
                onChange={handleInputChange}
              />
              <label htmlFor="mustard">Mustard</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="ketchup"
                name="sauces"
                value="ketchup"
                checked={formData.sauces.includes("ketchup")}
                onChange={handleInputChange}
              />
              <label htmlFor="ketchup">Ketchup</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="mayo"
                name="sauces"
                value="mayo"
                checked={formData.sauces.includes("mayo")}
                onChange={handleInputChange}
              />
              <label htmlFor="mayo">Mayo</label>
            </div>
          </div>
        </div>
        <div className="dataRow">
          <div className="labelName">
            <label>What is your favorite Stooge?</label>
          </div>
          <div className="inputData">
            <div>
              <label>
                <input
                  type="radio"
                  name="bestStooge"
                  value="Larry"
                  checked={formData.bestStooge === "Larry"}
                  onChange={handleInputChange}
                />
                Larry
              </label>
            </div>
            <div>
              <label>
                <input
                  type="radio"
                  name="bestStooge"
                  value="Moe"
                  checked={formData.bestStooge === "Moe"}
                  onChange={handleInputChange}
                />
                Moe
              </label>
            </div>
            <div>
              <label>
                <input
                  type="radio"
                  name="bestStooge"
                  value="Curly"
                  checked={formData.bestStooge === "Curly"}
                  onChange={handleInputChange}
                />
                Curly
              </label>
            </div>
          </div>
        </div>
        <div className="dataRow">
          <div className="labelName">
            <label>
              Notes (max 100 characters):
            </label>
          </div>
          <div className="inputData">
            <textarea
              placeholder="Notes"
              name="notes"
              value={formData.notes}
              onChange={handleInputChange}
              className={formErrors.notes ? "error" : ""}
            />
            {formErrors.notes && (
              <p className="error-message">Notes must be less than 100 characters</p>
            )}
          </div>
          
        </div>
        <div className="buttons">
          <button disabled={!formChanged} className="submit" type="submit">Submit</button>
          <button disabled={!formChanged} className="reset" type="button" onClick={handleFormReset}>
            Reset
          </button>
        </div>
      </form>
      <FormSummary formData={formData}/>
    </div>
  )}

  export default UserForm